#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h> 

#define fatal(msg, n)                         \
	{                                         \
		fprintf(stderr, "%s (%d)\n", msg, n); \
		exit(1);                              \
	}

/*
The header of a wav file Based on:
https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
*/
typedef struct wavfile_header_s {
	char ChunkID[4]; /*  4   */
	int32_t ChunkSize; /*  4   */
	char Format[4]; /*  4   */

	char Subchunk1ID[4]; /*  4   */
	int32_t Subchunk1Size; /*  4   */
	int16_t AudioFormat; /*  2   */
	int16_t NumChannels; /*  2   */
	int32_t SampleRate; /*  4   */
	int32_t ByteRate; /*  4   */
	int16_t BlockAlign; /*  2   */
	int16_t BitsPerSample; /*  2   */

	char Subchunk2ID[4];
	int32_t Subchunk2Size;
} wavfile_header_t;

/*Standard values for CD-quality audio*/
#define SUBCHUNK1SIZE (16)
#define AUDIO_FORMAT (1) /*For PCM*/
#define NUM_CHANNELS (2)
#define SAMPLE_RATE (44100)

#define BITS_PER_SAMPLE (16)

#define BYTE_RATE (SAMPLE_RATE * NUM_CHANNELS * BITS_PER_SAMPLE / 8)
#define BLOCK_ALIGN (NUM_CHANNELS * BITS_PER_SAMPLE / 8)

/*Return 0 on success and -1 on failure*/
int write_PCM16_stereo_header(FILE* file_p,
	int32_t SampleRate,
	int32_t FrameCount) {
	int ret;

	wavfile_header_t wav_header;
	int32_t subchunk2_size;
	int32_t chunk_size;

	size_t write_count;

	subchunk2_size = FrameCount * NUM_CHANNELS * BITS_PER_SAMPLE / 8;
	chunk_size = 4 + (8 + SUBCHUNK1SIZE) + (8 + subchunk2_size);

	wav_header.ChunkID[0] = 'R';
	wav_header.ChunkID[1] = 'I';
	wav_header.ChunkID[2] = 'F';
	wav_header.ChunkID[3] = 'F';

	wav_header.ChunkSize = chunk_size;

	wav_header.Format[0] = 'W';
	wav_header.Format[1] = 'A';
	wav_header.Format[2] = 'V';
	wav_header.Format[3] = 'E';

	wav_header.Subchunk1ID[0] = 'f';
	wav_header.Subchunk1ID[1] = 'm';
	wav_header.Subchunk1ID[2] = 't';
	wav_header.Subchunk1ID[3] = ' ';

	wav_header.Subchunk1Size = SUBCHUNK1SIZE;
	wav_header.AudioFormat = AUDIO_FORMAT;
	wav_header.NumChannels = NUM_CHANNELS;
	wav_header.SampleRate = SampleRate;
	wav_header.ByteRate = BYTE_RATE;
	wav_header.BlockAlign = BLOCK_ALIGN;
	wav_header.BitsPerSample = BITS_PER_SAMPLE;

	wav_header.Subchunk2ID[0] = 'd';
	wav_header.Subchunk2ID[1] = 'a';
	wav_header.Subchunk2ID[2] = 't';
	wav_header.Subchunk2ID[3] = 'a';
	wav_header.Subchunk2Size = subchunk2_size;

	write_count = fwrite(&wav_header,
		sizeof(wavfile_header_t), 1,
		file_p);

	ret = (1 != write_count) ? -1 : 0;

	return ret;
}

/*Data structure to hold a single frame with two channels*/
typedef struct PCM16_stereo_s {
	int16_t left;
	int16_t right;
} PCM16_stereo_t;

PCM16_stereo_t* allocate_PCM16_stereo_buffer(int32_t FrameCount) {
	return (PCM16_stereo_t*)malloc(sizeof(PCM16_stereo_t) * FrameCount);
}

/*Return the number of audio frames sucessfully written*/
size_t write_PCM16wav_data(FILE* file_p,
	int32_t FrameCount,
	PCM16_stereo_t* buffer_p) {
	size_t ret;

	ret = fwrite(buffer_p,
		sizeof(PCM16_stereo_t), FrameCount,
		file_p);

	return ret;
}

typedef enum {
	// for no token
	NONE,

	// keywords
	// "let"
	LET,
	// "for"
	FOR,
	// "in"
	IN,
	// "out
	OUT,

	// sigils
	// "("
	L_PAREN,
	// ")"
	R_PAREN,
	// "{"
	L_BRACKET,
	// "}"
	R_BRACKET,
	// "="
	EQUALS,
	// "=>"
	FAT_ARROW,
	// ".."
	RANGE,

	// operators
	// "+"
	PLUS,
	// "-"
	MINUS,
	// "*"
	MUL,
	// "/"
	DIV,

	// variable size
	IDENT,
	STRING,
	NUMBER,
	FLOAT,
	SPACE,

	// EOF
	TEOF,
} nava_token_type_t;

const char* token_names[] = { "NONE", "let", "for", "in", "out", "L_PAREN", "R_PAREN", "L_BRACKET", "R_BRACKET", "EQUALS", "FAT_ARROW", "RANGE", "PLUS", "MINUS", "MUL", "DIV", "IDENT", "STRING", "NUMBER", "FLOAT", "SPACE", "TEOF" };

struct nava_token {
	nava_token_type_t type;
	char* raw;
};
typedef struct nava_token nava_token_t;

// S expr type
typedef enum {
	S_NONE,
	S_ATOM,
	S_CONS,
} nava_s_type_t;

const char* s_names[] = { "NONE", "ATOM", "CONS" };

// S expr
struct nava_s {
	nava_s_type_t type;
	struct {
		char* name;
	} atom;
	struct {
		double value;
	} number;
	struct {
		char* atom;
		struct nava_s** cons;
	} cons;
};
typedef struct nava_s nava_s_t;

#define NEWTOK(token, ty, r) \
	{                        \
		token.type = ty;     \
		token.raw = r;       \
	}

#define NEWTOK_C(token, ty, c)                                \
	{                                                         \
		char str[2] = { c, '\0' };                            \
		token.type = ty;                                      \
		token.raw = malloc(2 * sizeof(char));                 \
		if (token.raw == NULL)                                \
			fatal("Memory error! malloc() returned NULL", 0); \
		strcpy(token.raw, str);                               \
	}

#define ADD_TOKEN(tokens, token, type, raw, idx) \
	{                                            \
		NEWTOK(token, type, raw);                \
		tokens[idx] = token;                     \
		idx++;                                   \
	}

#define ADD_TOKEN_C(tokens, token, type, c, idx) \
	{                                            \
		NEWTOK_C(token, type, c);                \
		tokens[idx] = token;                     \
		idx++;                                   \
	}

char peek(FILE* file) {
	char c = fgetc(file);
	ungetc(c, file);
	return c;
}

int parse(FILE* file, nava_token_t* tokens, int size) {
	char c, p;
	nava_token_t token;
	int idx = 0;
	int char_n = 0;

	while ((c = fgetc(file)) != EOF) {
		// grow the list
		if (idx >= size) {
			size *= 2;
			tokens = realloc(tokens, size * sizeof(nava_token_t));
			if (tokens == NULL)
				fatal("Memory error! realloc() returned NULL", char_n);
		}

		switch (c) {
		case EOF:
			return idx;
			break;

		case '+': {
			ADD_TOKEN_C(tokens, token, PLUS, c, idx);
			break;
		}

		case '-': {
			ADD_TOKEN_C(tokens, token, MINUS, c, idx);
			break;
		}

		case '/': {
			ADD_TOKEN_C(tokens, token, DIV, c, idx);
			break;
		}

		case '*': {
			ADD_TOKEN_C(tokens, token, MUL, c, idx);
			break;
		}

		case '(': {
			ADD_TOKEN_C(tokens, token, L_PAREN, c, idx);
			break;
		}

		case ')': {
			ADD_TOKEN_C(tokens, token, R_PAREN, c, idx);
			break;
		}

		case '{': {
			ADD_TOKEN_C(tokens, token, L_BRACKET, c, idx);
			break;
		}

		case '}': {
			ADD_TOKEN_C(tokens, token, R_BRACKET, c, idx);
			break;
		}

		case '.': {
			p = peek(file);

			// a range
			if (p == '.') {
				// actually consume the char
				p = fgetc(file);

				// create our string
				char* str = malloc(3 * sizeof(char));
				if (str == NULL)
					fatal("Memory error! malloc() returned NULL", 0);

				// build it
				str[0] = c;
				str[1] = p;
				str[2] = '\0';

				ADD_TOKEN(tokens, token, RANGE, str, idx);
			} else {
				fprintf(stderr, "Syntax error: token \"%c\" invalid! (%d)\n", c, idx);
				exit(1);
			}

			break;
		}

		case '=': {
			p = peek(file);
			// fat arrow
			if (p == '>') {
				// actually consume the char
				p = fgetc(file);

				// create our string
				char* str = malloc(3 * sizeof(char));
				if (str == NULL)
					fatal("Memory error! malloc() returned NULL", 0);

				// build it
				str[0] = c;
				str[1] = p;
				str[2] = '\0';

				ADD_TOKEN(tokens, token, FAT_ARROW, str, idx);
			} else {
				// a simple equals token
				ADD_TOKEN_C(tokens, token, EQUALS, c, idx);
			}

			break;
		}

		default: {
			if (!isspace(c)) {
				// an identifier
				if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
					int i = 1;
					int size = 8;

					// create a string buffer
					char* str = malloc(size * sizeof(char));
					if (str == NULL)
						fatal("Memory error! malloc() returned NULL", 0);

					// set the first character
					str[0] = c;

					// loop over the rest of the ident
					while ((c = fgetc(file)) != EOF) {
						// extend our string if the idx is too big
						if (i >= (size - 1)) {
							size *= 2;
							str = realloc(str, size * sizeof(char));
						}

						// make sure we have a valid ident
						if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '_')) {
							str[i] = c;
						} else {
							break;
						}

						i++;
					}

					str[i + 1] = '\0';

					nava_token_type_t type = IDENT;
					if (strcmp("let", str) == 0) {
						type = LET;
					} else if (strcmp("in", str) == 0) {
						type = IN;
					} else if (strcmp("for", str) == 0) {
						type = FOR;
					} else if (strcmp("out", str) == 0) {
						type = OUT;
					}

					ADD_TOKEN(tokens, token, type, str, idx);
				} else if (c == '"') {
					int i = 0;
					int size = 8;

					// create a string buffer
					char* str = malloc(size * sizeof(char));
					if (str == NULL)
						fatal("Memory error! malloc() returned NULL", 0);

					// we skip the quote here

					while ((c = fgetc(file)) != EOF) {
						if (i >= (size - 1)) {
							size *= 2;
							str = realloc(str, size * sizeof(char));
						}

						// check for an end quote
						if (c == '"') {
							break;
						} else {
							str[i] = c;
						}

						i++;
					}

					str[i + 1] = '\0';
					ADD_TOKEN(tokens, token, IDENT, str, idx);
				} else if (isdigit(c)) {
					int i = 1;
					int size = 8;
					nava_token_type_t type = NUMBER;

					// create a string buffer
					char* str = malloc(size * sizeof(char));
					if (str == NULL)
						fatal("Memory error! malloc() returned NULL", 0);

					// add the first number
					str[0] = c;

					while ((c = fgetc(file)) != EOF) {
						if (i >= (size - 1)) {
							size *= 2;
							str = realloc(str, size * sizeof(char));
						}

						if (c == '.') {
							if (type == FLOAT) {
								fatal("Syntax error: too many \".\"s in number", idx);
							}
							type = FLOAT;
							str[i] = c;
						} else if (!isdigit(c)) {
							break;
						} else {
							str[i] = c;
						}

						i++;
					}

					str[i + 1] = '\0';
					ADD_TOKEN(tokens, token, type, str, idx);
				} else {
					fprintf(stderr, "Syntax error: token \"%c\" invalid! (%d)\n", c, char_n);
					exit(1);
				}
			}
			break;
		}
		}

		char_n++;
	}

	return idx;
}

void free_tokens(nava_token_t* tokens, int size) {
	for (int i = 0; i < size; i++) {
		free(tokens[i].raw);
	}
}

typedef struct {
	int rh;
	int lh;
} nava_binding_power_t;

nava_binding_power_t infix_binding_power(nava_token_t op) {
	nava_binding_power_t power;
	switch (op.type) {
		case PLUS:
		case MINUS: {
			power.lh = 1;
			power.rh = 2;
			break;
		}

		case MUL:
		case DIV: {
			power.lh = 3;
			power.rh = 4;
			break;
		}

		case EQUALS: {
			power.lh = 5;
			power.rh = 6;
			break;
		}

		default: {
			fprintf(stderr, "Parsing error: bad infix op \"%s\"; cannot find bind power\n", op.raw);
			exit(1);
			break;
		}
	}

	return power;
}

typedef struct {
	int idx;
} nava_state_t;

nava_s_t* expr_bp(nava_state_t* state, nava_token_t* tokens, int min_bp) {
	nava_s_t* lhs;
	nava_token_t op;

	lhs = malloc(sizeof(nava_s_t));
	if (lhs == NULL) fatal("Memory error! malloc() returned NULL", 0);

	nava_token_t next = tokens[state->idx];
	switch (next.type) {
		case NUMBER:
		case FLOAT:
		case IDENT: {
			lhs->type = S_ATOM;
			lhs->atom.name = malloc(strlen(next.raw) * sizeof(char));
			if (lhs->atom.name == NULL) fatal("Memory error! malloc() returned NULL", 0);
			strcpy(lhs->atom.name, next.raw);
			break;
		}

		default: {
			fprintf(stderr, "Parsing error: invalid token \"%s\" type \"%s\"\n", next.raw, token_names[next.type]);
			exit(1);
			break;
		}
	}

	state->idx++;

	bool loop = true;
	while (loop) {
		printf("idx: %d, ", state->idx);
		next = tokens[state->idx];
		printf("next.type: %s\n", token_names[next.type]);

		// because "break" in a switch doesn't actually break the loop
		if (next.type == TEOF) {
			loop = false;
			break;
		}

		switch (next.type) {
			case PLUS:
			case MINUS:
			case MUL:
			case DIV:
			case EQUALS: {
				op = next;
				break;
			}

			default: {
				fprintf(stderr, "Parsing error: invalid token \"%s\"\n", next.raw);
				exit(1);
				break;
			}
		}

		nava_binding_power_t bp = infix_binding_power(op);
		int r_bp = bp.rh, l_bp = bp.lh;
		if (l_bp < min_bp) {
			loop = false;
			break;
		}

		state->idx++;
		nava_s_t* rhs = expr_bp(state, tokens, r_bp);

		nava_s_t* tmp;
		if ((tmp = malloc(sizeof(nava_s_t))) == NULL) fatal("Memory error! malloc() returned NULL", 0);
		tmp->type = S_CONS;
		if ((tmp->cons.atom = malloc(strlen(op.raw) * sizeof(char))) == NULL) fatal("Memory error! malloc() returned NULL", 0);
		strcpy(tmp->cons.atom, op.raw);

		if ((tmp->cons.cons = malloc(2 * sizeof(nava_s_t*))) == NULL) fatal("Memory error! malloc() returned NULL", 0);
		memcpy(tmp->cons.cons[0], lhs, sizeof(nava_s_t));
		memcpy(tmp->cons.cons[1], rhs, sizeof(nava_s_t));

		lhs = tmp;
	}

	return lhs;
}

void print_s(nava_s_t* s, int tabs) {
	printf("%d\n", tabs);
	if (s->type == S_CONS) {
		print_s(s->cons.cons[0], tabs + 1);
		print_s(s->cons.cons[1], tabs + 1);
	} else if (s->type == S_ATOM) {
		for (int i = 0; i < tabs; i++) printf("\t");
		printf("%s\n", s->atom.name);
	}
}

int main(int argc, char** argv) {
	FILE* file;
	nava_token_t* tokens;
	int size = 32;

	if (argc < 2) {
		fprintf(stderr, "Please enter a file to read\n");
		return 1;
	}

	file = fopen(argv[1], "r");
	if (file == NULL) {
		fprintf(stderr, "Failed to open file \"%s\"\n", argv[1]);
		return 1;
	}

	tokens = malloc((size + 1) * sizeof(nava_token_t));
	if (tokens == NULL)
		fatal("Memory error! malloc() returned NULL", 0);

	int list_size = parse(file, tokens, size);

	// add TEOF
	nava_token_t teof;
	teof.type = TEOF;
	char* raw = "TEOF";
	if ((teof.raw = malloc(strlen(raw) * sizeof(char))) == NULL) fatal("Memory error! malloc() returned NULL", 0);
	strcpy(teof.raw, raw);
	tokens[list_size++] = teof;

	printf("list size: %d\n", list_size);
	for (int i = 0; i < list_size; i++) {
		printf("i: %d, type: %s, raw: \"%s\"\n", i, token_names[tokens[i].type], tokens[i].raw);
	}

	nava_state_t* state = malloc(sizeof(nava_state_t));
	if (state == NULL) fatal("Memory error! malloc() returned NULL", 0);
	state->idx = 0;
	printf("Parsing exprs...\n");
	nava_s_t* s = expr_bp(state, tokens, 0);
	printf("OK\n");

	printf("Printing S exprs:\n");
	print_s(s, 0);
	printf("OK\n");

	free_tokens(tokens, list_size);
	free(tokens);
	free(state);
	free(s);

	return 0;
}
