CC = clang
CFLAGS = -Wall

all: nava

nava: nava.c
	${CC} -o nava nava.c ${CFLAGS}

clean:
	rm -f *.core nava *.wav

fmt:
	clang-format -i nava.c

test: all
	@./nava test.nava
